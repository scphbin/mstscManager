﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mstscManager
{
    class model
    {
    }

    public class AddModel {
        public string Name { get; set; }
        public string Ip { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Remarks { get; set; }
    }
}
