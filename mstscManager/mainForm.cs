﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mstscManager
{
    public partial class mainForm : Form
    {
        private static string DataPathXml = @"data.xml";
        private static List<AddModel> dataList = null;

        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            mainDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            mainDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            if (dataList == null) {
                dataList = new List<AddModel>();
            }
            if (File.Exists(DataPathXml)) {
                var data =  helps.DataRead(DataPathXml);
                foreach (var item in data) {
                    int index = mainDataGridView.Rows.Add();
                    mainDataGridView.Rows[index].Cells[0].Value = item.Name;
                    mainDataGridView.Rows[index].Cells[1].Value = item.Ip + ":" + item.Port;
                    mainDataGridView.Rows[index].Cells[2].Value = item.UserName;
                    mainDataGridView.Rows[index].Cells[3].Value = item.PassWord;
                    mainDataGridView.Rows[index].Cells[4].Value = item.Remarks;
                }

            }
        }


        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addMstsc();
        }
        private void addToolStripButton_Click(object sender, EventArgs e)
        {
            addMstsc();
        }

        private void delToolStripButton_Click(object sender, EventArgs e)
        {
            delMstsc();
        }

        private void mainDataGridView_DoubleClick(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dr in mainDataGridView.SelectedRows)
            {
                if (dr.IsNewRow == false) {
                    //connectMstsc(dr.Cells[1].Value.ToString(), dr.Cells[2].Value.ToString(), dr.Cells[3].Value.ToString());
                    var ipport = dr.Cells[1].Value.ToString().Split(':');
                    connectMstsc(dr.Cells[0].Value.ToString(), new Size(1024, 768), ipport[0], Convert.ToInt32(ipport[1]), dr.Cells[2].Value.ToString(),dr.Cells[3].Value.ToString());
                }

            }
        }


        /// <summary>
        /// 添加远程桌面
        /// </summary>
        private void addMstsc() {
            
            using (addForm af = new addForm()) {
                var obj = af.ShowDialog();
                if (obj != null)
                {
                    int index = mainDataGridView.Rows.Add();
                    mainDataGridView.Rows[index].Cells[0].Value = obj.Name;
                    mainDataGridView.Rows[index].Cells[1].Value = obj.Ip + ":" + obj.Port;
                    mainDataGridView.Rows[index].Cells[2].Value = obj.UserName;
                    mainDataGridView.Rows[index].Cells[3].Value = obj.PassWord;
                    mainDataGridView.Rows[index].Cells[4].Value = obj.Remarks;

                    dataList.Add(obj);
                    helps.DataWrite(dataList, DataPathXml);
                }
            }
            
        }


        /// <summary>
        /// 删除远程桌面
        /// </summary>
        private void delMstsc() {
            foreach (DataGridViewRow dr in mainDataGridView.SelectedRows)
            {
                if (dr.IsNewRow == false)
                    mainDataGridView.Rows.Remove(dr);
            }

        }


        /// <summary>
        /// 连接远程桌面
        /// </summary>
        private void connectMstsc(string ip, string username, string password) {

          string hashPassword =  helps.HashPassword(password);
          string rdpPath = helps.rdp(ip, username, hashPassword);
          helps.ExecuteInCmd("mstsc " + rdpPath);

        }

        private void connectMstsc(string title, Size size, string ip, int port, string username, string password) {
            var mstsc = new desktopForm();
            mstsc.title = title;
            mstsc.size = size;
            mstsc.server = ip;
            mstsc.port = port;
            mstsc.username = username;
            mstsc.password = password;
            mstsc.Show();
        }
    }
}
