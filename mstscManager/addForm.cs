﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mstscManager
{
    public partial class addForm : Form
    {
        private static AddModel addObject = null;
        public addForm()
        {
            InitializeComponent();
        }

        public new AddModel ShowDialog()
        {
            var oldRet = base.ShowDialog();
            if (oldRet == DialogResult.OK)
                return addObject;
            else
                return null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            addObject = new AddModel();
            addObject.Name = textBox1.Text;
            addObject.Ip = maskedTextBox1.Text;
            addObject.Port = textBox5.Text;
            addObject.UserName = textBox2.Text;
            addObject.PassWord = textBox3.Text;
            addObject.Remarks = textBox4.Text;

            this.DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
