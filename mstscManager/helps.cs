﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace mstscManager
{
    class helps
    {

        /// <summary>
        /// 读取xml
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<AddModel> DataRead(string path)
        {
           List<AddModel> xml = new List<AddModel>();
            using (StreamReader sr = new StreamReader(path))
            {
                XmlSerializer xmldes = new XmlSerializer(xml.GetType());
                Stream st = sr.BaseStream;
                xml = (List<AddModel>)xmldes.Deserialize(st);
                sr.Close();
            }

            return xml;
        }


        /// <summary>
        /// 写入xml
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="path"></param>
        public static void DataWrite(List<AddModel> xml, string path)
        {
            XmlSerializer xs = new XmlSerializer(xml.GetType());
            TextWriter tw = new StreamWriter(path);
            xs.Serialize(tw, xml);
            tw.Close();
        }


        /// <summary>
        /// 执行内部命令（cmd.exe 中的命令）
        /// </summary>
        /// <param name="cmdline">命令行</param>
        /// <returns>执行结果</returns>
        public static string ExecuteInCmd(string cmdline)
        {
            using (var process = new Process())
            {
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.CreateNoWindow = true;

                process.Start();
                process.StandardInput.AutoFlush = true;
                process.StandardInput.WriteLine(cmdline + "&exit");

                //获取cmd窗口的输出信息  
                string output = process.StandardOutput.ReadToEnd();

                process.WaitForExit();
                process.Close();

                return output;
            }
        }


        /// <summary>
        /// 生成rdp文件
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="userName"></param>
        /// <param name="password">加密过的密码</param>
        /// <returns></returns>
        public static string rdp( string ip, string userName, string password) {
            string path = @"temp.rdp";
            if (File.Exists(path)) File.Delete(path);
            using (StreamWriter file = new StreamWriter(path, true))
            {
                file.WriteLine("screen mode id:i:1");
                file.WriteLine("desktopwidth:i:1280");
                file.WriteLine("desktopheight:i:750");
                file.WriteLine("session bpp:i:24");
                file.WriteLine("winposstr:s:2,3,188,8,1062,721");
                file.WriteLine("full address:s:" + ip);
                file.WriteLine("compression:i:1");
                file.WriteLine("keyboardhook:i:2");
                file.WriteLine("audiomode:i:0");
                file.WriteLine("redirectdrives:i:0");
                file.WriteLine("redirectprinters:i:0");
                file.WriteLine("redirectcomports:i:0");
                file.WriteLine("redirectsmartcards:i:0");
                file.WriteLine("displayconnectionbar:i:1");
                file.WriteLine("autoreconnection");
                file.WriteLine("enabled:i:1");
                file.WriteLine("username:s:" + userName);
                file.WriteLine("alternate shell:s:");
                file.WriteLine("shell working directory:s:");
                file.WriteLine("password 51:b:" + password);
                file.WriteLine("disable wallpaper:i:1");
                file.WriteLine("disable full window drag:i:1");
                file.WriteLine("disable menu anims:i:1");
                file.WriteLine("disable themes:i:0");
                file.WriteLine("disable cursor setting:i:0");
                file.WriteLine("bitmapcachepersistenable:i:1");
            }
            return Directory.GetCurrentDirectory() + "\\" + path;
        }









        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "SendMessageA")]
        public static extern int SendMessage(int hwnd, int wMsg, int wParam, Byte[] lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessageA")]
        public static extern int SendMessage(int hwnd, int wMsg, int wParam, int lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);

        [DllImport("User32.dll ")]
        public static extern IntPtr FindWindowEx(IntPtr parent, IntPtr childe, string strclass, string FrmText);

        const int WM_SETTEXT = 0x000C;
        const int WM_LBUTTONDOWN = 0x0201;
        const int WM_LBUTTONUP = 0x0202;
        const int WM_CLOSE = 0x0010;

        const int WM_GETTEXT = 0x000D;
        const int WM_GETTEXTLENGTH = 0x000E;

        /// <summary>
        /// 加密密码
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string HashPassword(string password) {
            using (var process = new Process())
            {
                process.StartInfo.FileName = "RDP.exe";
                process.Start();

                Thread.Sleep(100);
                IntPtr rdpHwnd = FindWindow(null, "RDP Password Hasher");
                Thread.Sleep(100);
                System.Windows.Forms.SendKeys.Send("+");
                Thread.Sleep(100);
                System.Windows.Forms.SendKeys.Send(password);
                Thread.Sleep(100);
                System.Windows.Forms.SendKeys.Send("{TAB}");
                Thread.Sleep(100);
                System.Windows.Forms.SendKeys.Send("{ENTER}");
                Thread.Sleep(100);
                System.Windows.Forms.SendKeys.Send("{TAB}");
                Thread.Sleep(100);
                System.Windows.Forms.SendKeys.Send("^C");
                Thread.Sleep(100);
                SendMessage(rdpHwnd, WM_CLOSE, IntPtr.Zero, "");
                process.WaitForExit();
                process.Close();
            }

            return System.Windows.Forms.Clipboard.GetText(System.Windows.Forms.TextDataFormat.Text); 
        }
    }
}
