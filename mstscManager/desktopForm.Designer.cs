﻿namespace mstscManager
{
    partial class desktopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(desktopForm));
            this.RdpClient = new AxMSTSCLib.AxMsRdpClient9();
            ((System.ComponentModel.ISupportInitialize)(this.RdpClient)).BeginInit();
            this.SuspendLayout();
            // 
            // RdpClient
            // 
            this.RdpClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RdpClient.Enabled = true;
            this.RdpClient.Location = new System.Drawing.Point(0, 0);
            this.RdpClient.Name = "RdpClient";
            this.RdpClient.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("RdpClient.OcxState")));
            this.RdpClient.Size = new System.Drawing.Size(403, 315);
            this.RdpClient.TabIndex = 0;
            // 
            // desktopForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 315);
            this.Controls.Add(this.RdpClient);
            this.Name = "desktopForm";
            this.ShowIcon = false;
            this.Text = "desktopForm";
            this.Load += new System.EventHandler(this.desktopForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RdpClient)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxMSTSCLib.AxMsRdpClient9 RdpClient;
    }
}