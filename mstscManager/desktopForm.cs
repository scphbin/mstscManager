﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mstscManager
{
    public partial class desktopForm : Form
    {
        public string title { get; set; }
        public Size size { get; set; }
        public string server { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public desktopForm()
        {
            InitializeComponent();
        }

        private void desktopForm_Load(object sender, EventArgs e)
        {
            this.Text = title;
            this.Size = size;
            RdpClient.Server = server;
            RdpClient.AdvancedSettings2.RDPPort = port;
            RdpClient.UserName = username;
            RdpClient.AdvancedSettings2.ClearTextPassword = password;
            RdpClient.Connect();
        }
    }
}
