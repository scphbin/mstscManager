# mstscManager

#### 介绍
windows远程桌面管理

1、将服务器 ip地址 端口 用户名 密码 填写进去

2、选中一行服务器并双击，不用填写用户名和密码可快速进入windows远程桌面

3、下载 [发行版](https://gitee.com/scphbin/mstscManager/releases/1.0.0.0)

![输入图片说明](https://images.gitee.com/uploads/images/2020/0424/164100_ba128398_20764.png "QQ图片20200424163610.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0424/164112_15acd958_20764.png "QQ截图20200424161357.png")